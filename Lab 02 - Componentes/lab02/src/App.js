import React, { Component } from 'react';
import './App.css';
import Contador from './components/Contador/Contador'; 

class App extends Component {
  render(){
      return(<div>  
        <Contador valor={6} />
        <hr />
        <Contador valor={0} />
      </div>);
  }

/*render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }*/
}

export default App;
