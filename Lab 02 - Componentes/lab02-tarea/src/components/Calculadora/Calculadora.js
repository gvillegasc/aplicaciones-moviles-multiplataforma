import React, {Component} from 'react';

export default class Calculadora extends Component {
	state = {
		val1: this.props.valor1,
		val2: this.props.valor2,
		rpta: ""
	}

	SumarHandler = () => {
		this.setState({
			rpta: "La suma de los números es: " + this.state.val1 +  this.state.val2
		})
	}

	RestarHandler = () => {
		this.setState({
			rpta: "La resta de los números es: " + (this.state.val1 -  this.state.val2)
		})
	}

	MultiplicarHandler = () => {
		this.setState({
			rpta: "La multiplicación de los números es: " + this.state.val1 *  this.state.val2
		})
	}

	DividirHandler = () => {
		this.setState({
			rpta: "La división de los números es: " + this.state.val1 /  this.state.val2
		})
	}

	PorcentajeHandler = () => {
		this.setState({
			rpta: "El porcentaje del número 2 al número 1  es: " + this.state.val2 * 100 / this.state.val1 + "%"
		})
	}	

	render(){
		return(
			<div>
			<h1>Calculadora</h1>
			<p>El valor del número 1 es {this.state.val1}</p>
			<p>El valor del número 2 es {this.state.val2}</p>
			<p>{this.state.rpta}</p>
			<button onClick={this.SumarHandler}>Sumar</button>
			<button onClick={this.RestarHandler}>Restar</button>
			<button onClick={this.MultiplicarHandler}>Multiplicar</button>
			<button onClick={this.DividirHandler}>Dividir</button>
			<button onClick={this.PorcentajeHandler}>Porcentaje</button>
			</div>)
			
		
	}

}