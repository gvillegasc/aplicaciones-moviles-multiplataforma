import React, {Component} from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, TextInput } from 'react-native';
import axios from'axios';


type Props = {};


export default class App extends Component<Props> {
  state = {
    Loading: false,
    data: []
  }
  componentDidMount(){
    this.setState({loading: true});
    axios({
      method: 'GET',
      url: 'https://yts.am/api/v2/list_movies.json'
    }).then(response => {
      this.setState({
        loading: false,
        data: response.data.data.movies
      });
    }).catch(err => {
      this.setState({loading: false});
      console.warn(err);
    })
  }

  onPressHandler = item => {
    alert(item.description_full);
  }

  renderHeader = () => {
    return(<TextInput
      style={{height: 40, borderColor: 'gray', borderWidth: 1}}
      onChangeText={this.searchHandler}
      value={this.state.text}
    />);
  }
  


  searchHandler = text => {
    this.setState({
      text: text
    }, () => {
      const newData = data.filter(item => {
        const itemData = `${item.title_long.toUpperCase()}`;  
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        data: newData
      });
    });
  }

  render(){
    let contenido = (<Text>
      Cargando, espere por favor...
    </Text>);
    if(!this.state.loading){
      contenido = (<FlatList
        keyExtractor={(item, index) => index+''}
        data={this.state.data}
        renderItem={({item,index}) => {
      return(<TouchableOpacity onPress={() => this.onPressHandler(item)}>
          <Text style={index%2===0?styles.ItemEven:styles.ItemUneven}>
            {item.title_long}
           </Text>
      </TouchableOpacity>)
        }}
        ListHeaderComponent={this.renderHeader}
      />);
    }
    return(<View>
      {contenido}
    </View>);
  }
  /*render() {
    return (
      <View>
       <FlatList
         keyExtractor = {(item,index) => index+''}
         data={this.state.data}
         renderItem={({item,index}) => {
           return (<TouchableOpacity onPress={() => this.onPressHandler(item)}>
           <Text style={index%2===0?styles.ItemEven:styles.ItemUneven}>
            {item.name}
           </Text>
          </TouchableOpacity>);
        }}
        ListHeaderComponent={this.renderHeader}
         />
      </View>);
  }*/
  
  }

const styles = StyleSheet.create({
  ItemEven: {
    color: '#2B486F'
  },
  ItemUneven: {
    backgroundColor: '#D46A6A',
    color: 'white'
  }
});
