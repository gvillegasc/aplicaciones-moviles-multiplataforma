﻿APLICACIONES MOVILES MULTIPLATAFORMA
	Laboratorio 05
	Alumno: Gerardo Josue Villegas Condori C-15 A

CONFIGURACION DE PROYECTO

1. Ejecución de proyecto creado en el celular (Punto 1.2. Imagen01.png)

2. Modificación de archivo "App.js" (Punto 1.4. Imagen02.png)

3. Ejecución de reloj (Punto 1.4. Imagen03.gif)


LISTADO DE REACT NATIVE

4. Ejecución de listado en el celular (Punto 2.1 Imagen04.png)

5. Ejecución de nuevo listado en celular (Punto 2.2. Imagen05.png)

6. Ejecución del listado sin la advertencia (Punto 2.3. Imagen06.png)

7. Ejecución de lista people (Punto 2.4. Imagen07.png)

8. Modificación de archivo "App.js" (Punto 2.5 Imagen08.gif)


AXIOS

9. Instalación de Axios (Punto 3.2. Imagen09.png)

10. Ejecución de aplicación para listar peliculas (Punto 3.4. Imagen10.gif)