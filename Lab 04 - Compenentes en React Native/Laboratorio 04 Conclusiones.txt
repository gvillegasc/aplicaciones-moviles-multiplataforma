CONCLUSIONES

	1. React Native se combina sin problemas con componentes escritos en Swift, Java o Objective-C.

	2. React Native utiliza el mismo diseño que React, lo que le permite componer una interfaz de usuario móvil rica utilizando componentes declarativos.

	3. React Native es un nicho reducido para librerías de estilos para componentes en comparación con el nicho de las librerías y kits de estilo tradicionales.

	4. React Native permite definir componentes como clases o funciones. 

	5. Cada componente tiene varios "métodos de ciclo de vida" que puede anular para ejecutar código en momentos específicos del proceso.
