import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import Calculadora from './src/components/Calculadora';


export default class App extends Component{
  state = {
    valorInicial: '0'
  }
  iniciarContadoresHandler = (texto) => {
    this.setState({
        valorInicial: texto
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Calculadora</Text>
        <Calculadora  valor1={10} valor2={5}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
