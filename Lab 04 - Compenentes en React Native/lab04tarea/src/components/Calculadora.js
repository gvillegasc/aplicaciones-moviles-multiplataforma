import React,{Component} from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';

class Calculadora extends Component{
    state = {
        valor1: this.props.valor1,
        valor2: this.props.valor2,
        rpta: ""
    }
    componentDidUpdate(oldProps,oldState){
        if(oldProps.valor!==this.props.valor && !isNaN(this.props.valor)){
            this.setState({
                valor: this.props.valor
            })
        }
    }
    sumarHandler = () => {
        this.setState({
            rpta: this.state.valor1 + this.state.valor2
        });
    }
    restarHandler = () => {
        this.setState({
            rpta: this.state.valor1 - this.state.valor2
        });
    }
    multiplicarHandler = () => {
        this.setState({
            rpta: this.state.valor1 * this.state.valor2
        });
    }
    dividirHandler = () => {
        this.setState({
            rpta: this.state.valor1 / this.state.valor2
        });
    }
    render(){
        return(<View>
                    <Text>Respuesta: {this.state.rpta}</Text>
            <Text>Valor nro 1: {this.state.valor1}</Text>
            <Text>Valor nro 2: {this.state.valor2}</Text>
            <Button 
                title='Sumar'
                onPress={this.sumarHandler}
                />
            <Button
                title='Restar'
                color="#841584"
                onPress={this.restarHandler}
                />
             <Button
                title='Multiplicar'
                color="#FF0000"
                onPress={this.multiplicarHandler}
                />
             <Button
                title='Dividir'
                color="#000000"
                onPress={this.dividirHandler}
                />
        </View>);
    }
}

export default Calculadora;