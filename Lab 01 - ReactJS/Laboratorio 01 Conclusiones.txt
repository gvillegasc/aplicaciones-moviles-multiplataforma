﻿CONCLUSIONES

	1. React es una biblioteca Javascript de código abierto diseñada para crear interfaces de usuario.

	2. React tiene el objetivo de facilitar el desarrollo de aplicaciones en una sola página.

	3. React es mantenido por Facebook y la comunidad de software libre, han participado en el proyecto más de mil desarrolladores diferentes.

	4. React es la Vista en un contexto en el que se use el patrón MVC.

	5. React intenta ayudar a los desarrolladores a construir aplicaciones que usan datos que cambian todo el tiempo.
