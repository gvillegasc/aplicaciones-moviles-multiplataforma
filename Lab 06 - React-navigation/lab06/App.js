import React from 'react';
import { View, Text, Button } from "react-native";
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/Home';
import DetailsScreen from './src/screens/Details';
import ProfileScreen from './src/screens/Profile';

const AppNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    path: 'home/',
    navigationOptions: {
      title: 'Esta es la Home'
    }
  },
  Details: DetailsScreen,
  Profile: ProfileScreen,
},
{
 initialRouteName: "Home",
 defaultNavigationOptions: {
   title: 'Un titulo genérico',
   headerStyle: {
     backgroundColor: '#f4511e',
   },
   headerTintColor: '#fff',
   headerTitleStyle: {
     fontWeight: 'bold',
   },
 },
});

export default createAppContainer(AppNavigator);

/*
class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center"}}>
        <Text>Hola mundo!</Text>
        <Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details') }></Button>
      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render(){
    return(
      <View style={{ flex: 1, alignItems: "center", justifyContent:"center"}}>
        <Text>Details Screen</Text>
        <Button
          title="Go to Home"
          onPress= {() => this.props.navigation.navigate('Home')}>
        </Button>
      </View>
    );
  }
}
*/
